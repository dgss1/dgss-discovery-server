package com.dgss.discovery;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class DgssDiscoveryServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DgssDiscoveryServerApplication.class, args);
	}

}
